#ifndef _SPROCKET_C_
#define _SPROCKET_C_

#include "SPRocket.h"


//SPRocket Constructor
SPRocket::SPRocket(Servo internalSPRocketServo){  
  SPRocketStateMachine = WAIT_BT_CONNECTION;
  internalServo = internalSPRocketServo;
  this->presentDevices=0x00;
  this->maxAltitude = 0;
  this->dataloggerTempMarkAltitude = 0;
  this->dataloggerTempMarkAccel = 0;
  this->dataloggerBufferIndex = 1;
}

//Bluetooth Device
void SPRocket::setBluetoothDevice(SoftwareSerial* inBluetoothDevice){
  bluetoothDevice=inBluetoothDevice;
}

void SPRocket::waitConnection(SoftwareSerial BluetoothSerial)
{  
  if (BluetoothSerial.available()){    
    while(BluetoothSerial.available()){
      byte bluetoothChar = BluetoothSerial.read();
      Serial.write(bluetoothChar);   
    }   
  }
}

byte SPRocket::parseCommand(SoftwareSerial BluetoothSerial)
{
  return 0x00;
}

void SPRocket::setGroundAltitude(short altitude)
{
  groundAltitude = altitude;
}

bool SPRocket::detectLiftOff(short altitude)
{
  unsigned long temporallyTempMark;
  if (altitude - groundAltitude > LIFTOFFTHRESHOLD){
    //Set current liftOff time
    missionTime = millis();
    liftOffTime = millis();
    //Set start of datalogger
    dataloggerTempMarkAltitude=millis();
    dataloggerTempMarkAccel=millis();
    //Insert first element in datalogger
    temporallyTempMark=(dataloggerTempMarkAltitude/100);//time in dec of seconds
    memcpy(&dataloggerData[dataloggerBufferIndex].typeTimestamp, &temporallyTempMark,2);
    dataloggerData[dataloggerBufferIndex].value=altitude - groundAltitude;
    dataloggerBufferIndex++;
    return true;
  }
  return false;
}

bool SPRocket::detectApogee(short altitude)
{
  if (altitude > maxAltitude){
    maxAltitude=altitude;
    return false;
  }
  if (maxAltitude - altitude > APOGEETHRESHOLD){
    //Anti noise filter height
    antinoiseHeight++;
    if(antinoiseHeight>ANTINOISEVALUE){
      return true;
    }else{
      return false;
    }
  }else{
    antinoiseHeight=0;
  }
  return false;
}

void SPRocket::setClimbTime()
{
  climbTime = millis()-liftOffTime;
}


bool SPRocket::detectLanding(short altitude)
{
  if (altitude == groundAltitude){
    missionTime = millis()-missionTime;
    return true;
  }
  return false;
}


short SPRocket::getCurrentAltitude(short altitude)
{
  return (altitude - groundAltitude);
}


byte SPRocket::getStateMachineStatus()
{
  return SPRocketStateMachine;
}

byte SPRocket::setStateMachineStatus(byte status)
{ 
  SPRocketStateMachine = status;
  return E_OK;
}

short SPRocket::getMaxAltitude()
{ 
  if (groundAltitude<=maxAltitude){
    return ((short)(maxAltitude-groundAltitude));
  }
  return 0;
}

short SPRocket::getMissionTime()
{ 
  return (short)missionTime/1000;
}

short SPRocket::getClimbTime()
{ 
  return (short)climbTime/1000;
}

byte SPRocket::parseReceivedCommand(byte command[10])
{
  #ifdef DEBUG_MODE
  Serial.println("parsing command...");
  #endif
  byte auxPointer;
  //Command hello
  if ((command[0]=='h')&&(command[1]=='e')){
    Serial.print(SPRocketStateMachine);
    #ifdef DEBUG_MODE
    Serial.println("received hello!!!\n");
    #endif
    return WAIT_BT_CONNECTION;
  }
  
  //Command arm_rocket
  if ((command[0]=='a')&&(command[1]=='r')){
    Serial.print(SPRocketStateMachine);
    #ifdef DEBUG_MODE
    Serial.println("received arm!!!\n");
    #endif
    return ARM_ROCKET;
  }
    
  //Command disarm_rocket
  if ((command[0]=='d')&&(command[1]=='i')){
    Serial.print(SPRocketStateMachine);
    #ifdef DEBUG_MODE
    Serial.println("received disarm!!!\n");
    #endif
    return DISARM_ROCKET;
  }   
  
  //Command launch_rocket
  if ((command[0]=='l')&&(command[1]=='a')){
    Serial.print(SPRocketStateMachine);
    #ifdef DEBUG_MODE
    Serial.println("received launch!!!\n");
    #endif
    return LAUNCH;
  }    
  
  //Command abort launch
  if ((command[0]=='a')&&(command[1]=='b')){
    Serial.print(SPRocketStateMachine);
    #ifdef DEBUG_MODE
    Serial.println("received abort launch!!!\n");
    #endif
    return ABORT_LAUNCH;
  }    

  //Command sendTelemetry index number
  if ((command[0]=='t')&&(command[1]=='n')){
    Serial.print(SPRocketStateMachine);
    #ifdef DEBUG_MODE
    Serial.println("rx send tele!!!\n");
    #endif
    return SEND_TELEMETRY_NUMBER;
  }   

  //Command sendTelemetry
  if ((command[0]=='s')&&(command[1]=='t')){
    Serial.print(SPRocketStateMachine);
    #ifdef DEBUG_MODE
    Serial.println("rx send tele!!!\n");
    #endif
    return SEND_TELEMETRY;
  }

}

//Datalogger functions
void SPRocket::registerDataloggerVoidValue(){
    unsigned long auxTempMark,auxtemporallyTempMark;
    dataloggerTempMarkAltitude=millis();
    //Insert first element in datalogger
    auxtemporallyTempMark=(dataloggerTempMarkAltitude/100);//time in dec of seconds
    memcpy(&dataloggerData[0].typeTimestamp, &auxtemporallyTempMark,2);
    dataloggerData[0].value=0;
}

void SPRocket::updateDatalogger(uint8_t stateMachineState, unsigned short value, uint8_t valueType){
  unsigned long temporallyTempMark;
  //Check if buffer is full
  if (dataloggerBufferIndex <= DATALOGGER_NUM_DATA){
    //check if datalogger must be logged
    switch (stateMachineState){
      case WAIT_APOGEE:         
        if (((millis()-dataloggerTempMarkAccel)>WAIT_APOGEE_FREQ)&&
          ((valueType==VALUETYPE_ACCELX)||(valueType==VALUETYPE_ACCELY)||(valueType==VALUETYPE_ACCELZ))){
          #ifdef DEBUG_MODE
          Serial.print("wa:");
          Serial.println(dataloggerBufferIndex);
          #endif
          //Set start of datalogger
          dataloggerTempMarkAccel=millis();
          //Insert first element in datalogger
          temporallyTempMark=(dataloggerTempMarkAccel/100);//time in dec of seconds
          temporallyTempMark = temporallyTempMark | (valueType << 13);
          memcpy(&dataloggerData[dataloggerBufferIndex].typeTimestamp, &temporallyTempMark,2);
          #ifdef DEBUG_MODE
          Serial.print ("wa set time:");
          Serial.println(dataloggerData[dataloggerBufferIndex].typeTimestamp);
          Serial.println(temporallyTempMark);
          #endif
          dataloggerData[dataloggerBufferIndex].value=value;
          dataloggerBufferIndex++;
        }
        if (((millis()-dataloggerTempMarkAltitude)>WAIT_APOGEE_FREQ)&& (valueType==VALUETYPE_ALTITUDE)){
          #ifdef DEBUG_MODE
          Serial.print("wa:");
          Serial.println(dataloggerBufferIndex);
          #endif
          //Set start of datalogger
          dataloggerTempMarkAltitude=millis();
          //Insert first element in datalogger
          temporallyTempMark=(dataloggerTempMarkAltitude/100);//time in dec of seconds
          //Does not care about typeTimestamp, as altitude has typeTimestamp==0x00
          memcpy(&dataloggerData[dataloggerBufferIndex].typeTimestamp, &temporallyTempMark,2);
          #ifdef DEBUG_MODE
          Serial.print ("wa set time:");
          Serial.println(dataloggerData[dataloggerBufferIndex].typeTimestamp);
          Serial.println(temporallyTempMark);
          #endif
          dataloggerData[dataloggerBufferIndex].value=value - groundAltitude;
          dataloggerBufferIndex++;
        }
      break;
      case RELEASE_PARACHUTE:
        #ifdef DEBUG_MODE
        Serial.print("rp:");
        Serial.println(dataloggerBufferIndex);
        #endif
        //Insert event release parachute
        dataloggerTempMarkAltitude=millis();
        //Insert first element in datalogger
        temporallyTempMark=(dataloggerTempMarkAltitude/100);//time in dec of seconds
        //set typeTimestamp
        temporallyTempMark = temporallyTempMark | (VALUETYPE_PARACHUTE << 13);
        memcpy(&dataloggerData[dataloggerBufferIndex].typeTimestamp, &temporallyTempMark,2);
        dataloggerData[dataloggerBufferIndex].value=value - groundAltitude;
        dataloggerBufferIndex++;
      break;
      case WAIT_LANDING:
        if (((millis()-dataloggerTempMarkAltitude)>WAIT_LAND_FREQ)&& (valueType==VALUETYPE_ALTITUDE)){
          #ifdef DEBUG_MODE
          Serial.print("wl:");
          Serial.println(dataloggerBufferIndex);
          #endif
          //Set start of datalogger
          dataloggerTempMarkAltitude=millis();
          //Insert first element in datalogger
          temporallyTempMark=(dataloggerTempMarkAltitude/100);//time in dec of seconds
          memcpy(&dataloggerData[dataloggerBufferIndex].typeTimestamp, &temporallyTempMark,2);
          //Update typeTimestamp
          #ifdef DEBUG_MODE
          Serial.print ("wl set time:");
          Serial.println(dataloggerData[dataloggerBufferIndex].typeTimestamp);
          Serial.println(temporallyTempMark);
          #endif
          dataloggerData[dataloggerBufferIndex].value=value - groundAltitude;
          dataloggerBufferIndex++;
        }

        if (((millis()-dataloggerTempMarkAccel)>WAIT_LAND_FREQ)&&
          ((valueType==VALUETYPE_ACCELX)||(valueType==VALUETYPE_ACCELY)||(valueType==VALUETYPE_ACCELZ))){
            #ifdef DEBUG_MODE
            Serial.print("wa:");
            Serial.println(dataloggerBufferIndex);
            #endif
            //Set start of datalogger
            dataloggerTempMarkAccel=millis();
            //Insert first element in datalogger
            temporallyTempMark=(dataloggerTempMarkAccel/100);//time in dec of seconds
            temporallyTempMark = temporallyTempMark | (valueType << 13);
            memcpy(&dataloggerData[dataloggerBufferIndex].typeTimestamp, &temporallyTempMark,2);
            #ifdef DEBUG_MODE
            Serial.print ("wa set time:");
            Serial.println(dataloggerData[dataloggerBufferIndex].typeTimestamp);
            Serial.println(temporallyTempMark);
            #endif
            dataloggerData[dataloggerBufferIndex].value=value;
            dataloggerBufferIndex++;
        }

      break;
      case SPLASHDOWN:
        #ifdef DEBUG_MODE
        Serial.print("la:");
        Serial.println(dataloggerBufferIndex);
        #endif
        //Insert event release parachute
        dataloggerTempMarkAltitude=millis();
        //Insert first element in datalogger
        temporallyTempMark=(dataloggerTempMarkAltitude/100);//time in dec of seconds
        //set typeTimestamp
        temporallyTempMark = temporallyTempMark | (VALUETYPE_LANDING << 13);
        memcpy(&dataloggerData[dataloggerBufferIndex].typeTimestamp, &temporallyTempMark,2);
        dataloggerBufferIndex++;
      break;
    }
  }
}

uint8_t SPRocket::getDataloggerBufferIndex(){
  return dataloggerBufferIndex;
}

uint16_t SPRocket::getDataloggerDataTimestamp(uint8_t index){
  uint16_t temporallyValue;
  temporallyValue = (dataloggerData[index].typeTimestamp) & CLEAR_TYPE;
  return temporallyValue;
}

uint8_t SPRocket::getDataloggerDataType(uint8_t index){
  uint8_t temporallyType;
  temporallyType = (dataloggerData[index].typeTimestamp) >> 13;
  return temporallyType;
}

uint8_t SPRocket::getDataloggerDataValue(uint8_t index){
  return dataloggerData[index].value;
}


#endif // _SPROCKET_C_
