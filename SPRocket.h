#ifndef _SPROCKET_H_
#define _SPROCKET_H_


//All defines here
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <Servo.h>

#include "SPRocketConfig.h"
#include "SPRocketVariables.h"
#include <inttypes.h>
#include <avr/pgmspace.h>

String floatToString( float n, int l, int d, boolean z);



class SPRocket{

public:
    SPRocket(Servo internalSPRocketServo);
    void setBluetoothDevice(SoftwareSerial* bluetoothDevice);    
    byte getStateMachineStatus();
    byte setStateMachineStatus(byte status);
    byte parseCommand(SoftwareSerial BluetoothSerial);
    void waitConnection(SoftwareSerial BluetoothSerial);
    byte parseReceivedCommand(byte command[10]);
    void setGroundAltitude(short altitude);
    short getCurrentAltitude(short altitude);
    bool detectLiftOff(short altitude);
    bool detectApogee(short altitude);
    bool detectLanding(short altitude);
    short getMaxAltitude();
    short getMissionTime();
    short getClimbTime();
    void setClimbTime();
    void registerDataloggerVoidValue();
    uint8_t getDataloggerBufferIndex();
    uint16_t getDataloggerDataTimestamp(uint8_t index); 
    uint8_t getDataloggerDataType(uint8_t index);
    void updateDatalogger(uint8_t stateMachineState, unsigned short value, uint8_t valueType);
    uint8_t getDataloggerDataValue(uint8_t index);

private:
    //Peripheral objects
    SoftwareSerial* bluetoothDevice; //Bluetooth

    unsigned int SPRocketStateMachine;
    byte presentDevices;
    Servo internalServo;
    short maxAltitude;
    short groundAltitude;
    unsigned long missionTime;
    unsigned long climbTime;
    unsigned long liftOffTime;
    //Datalogger data
    dataloggerDataType dataloggerData[DATALOGGER_NUM_DATA];
    uint8_t dataloggerBufferIndex;
    unsigned long dataloggerTempMarkAltitude;
    unsigned long dataloggerTempMarkAccel;
    uint8_t antinoiseHeight;

};



#endif // _SPROCKET_H_

