/******************************************************************************/
/*                                                                            */
/*   Furgalladas Schalter und Sensoren                                        */
/*                                                                            */
/*   All rights reserved. Distribution or duplication without previous        */
/*   written agreement of the owner prohibited.                               */
/*                                                                            */
/******************************************************************************/

/** \file

   SPRocket - Water Rocket onboard control System


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> SPRocket.c                 </td></tr>
   <tr> <td> Revision: </td> <td> 1.5                        </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                   </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho            </td></tr>
   <tr> <td> Date:     </td> <td> 22-APRIL-2019 15:39:56     </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> SPRocket </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/* Bluetooth module HC-05
   RX is digital pin 8 of arduino (connect to TX of HC05)
   TX is digital pin 7 of arduino (connect to RX of HC05)
   Default baudRate: 9600. 
   No AT commands are transmitted, only used vÃ­a software UART
   Pairing code: 1234
*/

/* Sd card
 ** SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4
 */

/* Accelerometer: Address: 0x68
 *    //GND - GND
 *    //VCC - VCC
 *    //SDA - Pin A4
 *    //SCL - Pin A5
 */

/* Servo
 *  Servo pin attached at pin 9
 */

 /* LEDs
 *  Ready Led attached at pin 4
 *  Parachute sensor Led attached at pin 5
 *  Landing Led attached at pin 10
 */

 /*
  * Altimeter - Remember to modify address to match 0x76!!!
  *  //GND - GND
  *  //VCC - VCC
  *  //SDA - Pin A4
  *  //SCL - Pin A5
  */

  
  /* Sensor pins 
  *
  * Pin 3 => ParachuteArmed pin
  * Pin A0 => Battery sensor
  */

#ifndef I_SPROCKET_C
#define I_SPROCKET_C
//Define IDE version
#define ARDUINO 101

#include "SPRocket.h"
#include <SoftwareSerial.h>
#include <Servo.h>
#include <SPI.h>
//Seems that there is no room for SD library
//#include <SD.h> 
#include "I2Cdev.h"
#include <Wire.h>
#include <Adafruit_BME280.h>
//gyro library
#include "MPU9250mini.h"

//Variables
byte command[10]; //encode received command
byte commandCounter;
byte commandReceived;
static byte sendCommandCounter = 0;

//Bluetooth serial port 
SoftwareSerial BluetoothSerial(BLUETOOTH_RX, BLUETOOTH_TX);

//Servo
Servo sprocketServo;  

//Altimeter
Adafruit_BME280 bme; 

//Gyro
MPU9250mini sprocketIMU(Wire,0x68);

//New rocket
SPRocket sprocket(sprocketServo);

//altitude command
char commandValueString[7];
float commandValueFloat;

void setup() {
  int count=0;
  //PinModes  
  pinMode(BLUETOOTH_RX, INPUT);
  pinMode(BLUETOOTH_TX, OUTPUT);
  pinMode(PARACHUTEARMED_PIN,INPUT);
  pinMode(LANDINGLED_PIN, OUTPUT);
  pinMode(PARACHLED_PIN, OUTPUT);
  pinMode(READYLED_PIN, OUTPUT);

  //Init bluetooth module port
  sprocket.setBluetoothDevice(&BluetoothSerial);  
  BluetoothSerial.begin(9600);

  //Main debugging port
  #ifdef DEBUG_MODE
  Serial.begin(9600);
  #endif

  //Servo attach
  sprocketServo.attach(SERVO_PIN);
  sprocketServo.write(SERVO_DISARMED);

  //Initialization leds
  digitalWrite(LANDINGLED_PIN,LOW);
  digitalWrite(PARACHLED_PIN,LOW);
  digitalWrite(READYLED_PIN,LOW);
  //Altimeter
  bool status;
  status = bme.begin();  
  if (!status) {
      #ifdef DEBUG_MODE
      Serial.println("Could not find a valid BME280 sensor, check wiring!");
      #endif
      while (1);
  }  

   //Gyro
  // start communication with IMU 
  if (sprocketIMU.begin() < 0) {
    #ifdef DEBUG_MODE
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    #endif
  }
}


void loop() {
  uint8_t iterator;
  unsigned short currentAltitude;
  //check state machine  
  switch (sprocket.getStateMachineStatus()) {
    case WAIT_BT_CONNECTION:
      #ifdef DEBUG_MODE
      Serial.println("WAIT_BT_CONNECTION\n");
      #endif
      if (BluetoothSerial.available()){
        #ifdef DEBUG_MODE
        Serial.println("rx somethng");
        #endif
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          #ifdef DEBUG_MODE
          Serial.write(bluetoothChar);
          #endif
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        if (commandReceived==HELLO){
          #ifdef DEBUG_MODE
          Serial.println("rx hello,sending ack");
          #endif
          //Received hello. Set state machine
          BluetoothSerial.write("he:0;");
          sprocket.setStateMachineStatus(DISARMED);
        }
      }
      delay(500);
      break;
    case DISARMED:
      #ifdef DEBUG_MODE
      Serial.println("DISARMED\n");
      #endif
      if (BluetoothSerial.available()){
        #ifdef DEBUG_MODE
        Serial.println("disarm, rx somethg");
        #endif
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          #ifdef DEBUG_MODE
          Serial.write(bluetoothChar);
          #endif
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        #ifdef DEBUG_MODE
        Serial.print("disarmed, command: ");        
        Serial.println(commandReceived);
        #endif
        if (commandReceived==ARM_ROCKET){
          #ifdef DEBUG_MODE
          Serial.println("Rocket armed. Ack arm rocket. Ready to launch\n");
          #endif
          //Received hello. Set state machine
          BluetoothSerial.write("ar:0;");
          sprocket.setStateMachineStatus(ARMED);
          sprocketServo.write(SERVO_ARMED);
        }
      }      
      delay(500);
      break;
    case ARMED:
      #ifdef DEBUG_MODE
      Serial.println("ARMED\n");
      #endif
      sendParachuteArmedSensor(sendCommandCounter);
      if (BluetoothSerial.available()){
        #ifdef DEBUG_MODE
        Serial.println("arm,tx something");
        #endif
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          #ifdef DEBUG_MODE
          Serial.write(bluetoothChar);
          #endif
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        if (commandReceived==DISARM_ROCKET){
          //Received disarm. Set state machine
          #ifdef DEBUG_MODE
          Serial.println ("Ack disarm\n");
          #endif
          BluetoothSerial.write("di:0;");
          sprocket.setStateMachineStatus(DISARMED);
          sprocketServo.write(SERVO_DISARMED);
          digitalWrite(PARACHLED_PIN,LOW);
        }
        if (commandReceived==LAUNCH){
          //Received launch. Set state machine
          #ifdef DEBUG_MODE
          Serial.println ("Ack launch\n");
          #endif
          BluetoothSerial.write("la:0;");
          sprocket.setStateMachineStatus(WAIT_LAUNCH);
          digitalWrite(READYLED_PIN,HIGH);
          sprocket.setGroundAltitude((short)bme.readAltitude(SEALEVELPRESSURE_HPA));
        }    
      }      
      delay(500);
      break;
    case WAIT_LAUNCH:
      #ifdef DEBUG_MODE
      Serial.println("WAIT_LAUNCH\n");
      #endif
      //Abort
      if (BluetoothSerial.available()){
        #ifdef DEBUG_MODE
        Serial.println("waitinglaunch, rx somethng");
        #endif
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          #ifdef DEBUG_MODE
          Serial.write(bluetoothChar);
          #endif
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        if (commandReceived==ABORT_LAUNCH){
          //Received abort launch
          #ifdef DEBUG_MODE
          Serial.println ("Ack abort launch\n");
          #endif
          BluetoothSerial.write("ab:0;");
          sprocket.setStateMachineStatus(ARMED);
          digitalWrite(READYLED_PIN,LOW);
          break;
        }
      }
      //print_values();
      sendTelemetry(sendCommandCounter);
      sendParachuteArmedSensor(sendCommandCounter); 
      
      //Serial.println(sprocket.getCurrentAltitude((short)bme.readAltitude(SEALEVELPRESSURE_HPA)));
      if (sprocket.detectLiftOff((short)bme.readAltitude(SEALEVELPRESSURE_HPA))){
        #ifdef DEBUG_MODE
        Serial.println ("LiftOff\n");
        #endif
        BluetoothSerial.write("li:0;");
        sprocket.setStateMachineStatus(WAIT_APOGEE);
      }else{
        //Not liftOff => register value 0 of datalogger
        sprocket.registerDataloggerVoidValue();
      }
      delay(150);
      break;
    case WAIT_APOGEE:
      #ifdef DEBUG_MODE
      Serial.println("WAIT_APOGEE\n");
      #endif
      currentAltitude=(unsigned short)bme.readAltitude(SEALEVELPRESSURE_HPA);
      //Update datalogger if neccesary
      sprocket.updateDatalogger(WAIT_APOGEE,currentAltitude,VALUETYPE_ALTITUDE);
      if (GYRO_AVAILABLE){
        sprocketIMU.readSensor();
        switch (SELECTED_AXIS){
          case XAXIS:
            sprocket.updateDatalogger(WAIT_APOGEE,(unsigned short)sprocketIMU.getAccelX_mss(),VALUETYPE_ACCELX);
          break;
          case YAXIS:
            sprocket.updateDatalogger(WAIT_APOGEE,(unsigned short)sprocketIMU.getAccelY_mss(),VALUETYPE_ACCELY);
          break;
          case ZAXIS:
            sprocket.updateDatalogger(WAIT_APOGEE,(unsigned short)sprocketIMU.getAccelZ_mss(),VALUETYPE_ACCELZ);
          break;
        } 
      }
      if (sprocket.detectApogee(currentAltitude)){
        #ifdef DEBUG_MODE
        Serial.println("Apogee detected!!!\n");
        #endif
        BluetoothSerial.write("ap:0;");
        sprocket.setStateMachineStatus(RELEASE_PARACHUTE);
      }
      break;
    case RELEASE_PARACHUTE:
      #ifdef DEBUG_MODE
      Serial.println("releasing parachute!!!\n");
      #endif
      currentAltitude=(unsigned short)bme.readAltitude(SEALEVELPRESSURE_HPA);
      sprocket.setStateMachineStatus(WAIT_LANDING);
      sprocket.setClimbTime();
      sprocketServo.write(SERVO_DISARMED);
      BluetoothSerial.write("pa:0;");
      digitalWrite(LANDINGLED_PIN,HIGH);
      sprocket.updateDatalogger(RELEASE_PARACHUTE,currentAltitude,VALUETYPE_PARACHUTE);
      break;
    case WAIT_LANDING:
      #ifdef DEBUG_MODE
      Serial.println("Wait landing\n");
      #endif
      currentAltitude=(unsigned short)bme.readAltitude(SEALEVELPRESSURE_HPA);
      sprocket.updateDatalogger(WAIT_LANDING,currentAltitude,VALUETYPE_ALTITUDE);
      if (GYRO_AVAILABLE){
        sprocketIMU.readSensor();
        switch (SELECTED_AXIS){
          case XAXIS:
            sprocket.updateDatalogger(WAIT_APOGEE,(unsigned short)sprocketIMU.getAccelX_mss(),VALUETYPE_ACCELX);
          break;
          case YAXIS:
            sprocket.updateDatalogger(WAIT_APOGEE,(unsigned short)sprocketIMU.getAccelY_mss(),VALUETYPE_ACCELY);
          break;
          case ZAXIS:
            sprocket.updateDatalogger(WAIT_APOGEE,(unsigned short)sprocketIMU.getAccelZ_mss(),VALUETYPE_ACCELZ);
          break;
        } 
      }
      if (sprocket.detectLanding(currentAltitude)){
        #ifdef DEBUG_MODE
        Serial.println("Landing!!!\n");
        #endif
        sprocket.setStateMachineStatus(SPLASHDOWN);
        delay(500);
      }
      break;
    case SPLASHDOWN:
      #ifdef DEBUG_MODE
      Serial.println("Splashdown!!!\n");
      #endif
      sprocket.setStateMachineStatus(WAIT_RESCUE);
      BluetoothSerial.write("sp:0;");
      sprocket.updateDatalogger(SPLASHDOWN,0x00,VALUETYPE_LANDING);
      digitalWrite(LANDINGLED_PIN,LOW);
      delay(500);
      break;
    case WAIT_RESCUE:
      //send max altitude      
      //sprintf(commandValueString, "%d", sprocket.getMaxAltitude());
      sendMissionRecords(sendCommandCounter);
      digitalWrite(LANDINGLED_PIN,HIGH);
      delay(500);
      digitalWrite(LANDINGLED_PIN,LOW);
      delay(500);
      if (BluetoothSerial.available()){
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          #ifdef DEBUG_MODE
          Serial.write(bluetoothChar);
          #endif
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        #ifdef DEBUG_MODE
        Serial.print("waitrescue,rx: ");        
        Serial.println(commandReceived);
        #endif
        if (commandReceived==SEND_TELEMETRY_NUMBER){
          //Received send telemetry
          BluetoothSerial.write("tn:");
          BluetoothSerial.print(sprocket.getDataloggerBufferIndex());
          BluetoothSerial.write(";");
          //Debug print in serial console telemetry values
          #ifdef DEBUG_MODE
          Serial.write("tn:");
          Serial.print(sprocket.getDataloggerBufferIndex());
          Serial.write(";");
          #endif
        }
        if (commandReceived==SEND_TELEMETRY){
          //Received send telemetry
          for (iterator=0; iterator<sprocket.getDataloggerBufferIndex(); iterator++){
            BluetoothSerial.write("st:");
            BluetoothSerial.print(iterator);
            BluetoothSerial.write(",");
            switch (sprocket.getDataloggerDataType(iterator)){
              case VALUETYPE_ALTITUDE:
                BluetoothSerial.write("h");
                break;
              case VALUETYPE_PARACHUTE:
                BluetoothSerial.write("p");
                break;
              case VALUETYPE_LANDING:
                BluetoothSerial.write("l");
                break;
              case VALUETYPE_ACCELX:
                BluetoothSerial.write("ax");
                break;
              case VALUETYPE_ACCELY:
                BluetoothSerial.write("ay");
                break;
              case VALUETYPE_ACCELZ:
                BluetoothSerial.write("az");
                break;
            }
            
            BluetoothSerial.write(",");            
            BluetoothSerial.print(sprocket.getDataloggerDataTimestamp(iterator));
            BluetoothSerial.write(",");
            BluetoothSerial.print(sprocket.getDataloggerDataValue(iterator));
            BluetoothSerial.write(";");
            //Debug print in serial console telemetry values
            #ifdef DEBUG_MODE
            Serial.print("st:");
            Serial.print(iterator);
            Serial.print(",");
            switch (sprocket.getDataloggerDataType(iterator)){
              case VALUETYPE_ALTITUDE:
                Serial.print("h");
                break;
              case VALUETYPE_PARACHUTE:
                Serial.print("p");
                break;
              case VALUETYPE_LANDING:
                Serial.print("l");
                break;
              case VALUETYPE_ACCELX:
                Serial.print("ax");
                break;
              case VALUETYPE_ACCELY:
                Serial.print("ay");
                break;
              case VALUETYPE_ACCELZ:
                Serial.print("az");
                break;
            }
            Serial.print(",");            
            Serial.print(sprocket.getDataloggerDataTimestamp(iterator));
            Serial.print(",");
            Serial.print(sprocket.getDataloggerDataValue(iterator));
            Serial.println(";");
            #endif
            delay(20); //Delay to flush bluetooth buffer
          }

        }
      }     
      break;
  }

  //Send battery level if state is other than 
  if ((sprocket.getStateMachineStatus()!=WAIT_BT_CONNECTION) && (sprocket.getStateMachineStatus()!=WAIT_APOGEE) &&
  (sprocket.getStateMachineStatus()!=RELEASE_PARACHUTE) && (sprocket.getStateMachineStatus()!=WAIT_LANDING)  
  ){
    sendBatteryLevel(sendCommandCounter);
  }
  //Update counter and reset it
  sendCommandCounter++;
  if (sendCommandCounter > 50){ 
    sendCommandCounter = 0;
  }
}

void sendMissionRecords(byte counter){
  if (counter ==10){
    sprintf(commandValueString, "%d", sprocket.getClimbTime());
    BluetoothSerial.print("ct:");
    BluetoothSerial.write(commandValueString);
    BluetoothSerial.print(";");
  }
  if (counter==15){
    sprintf(commandValueString, "%d", sprocket.getMaxAltitude());
    BluetoothSerial.print("ma:");
    BluetoothSerial.write(commandValueString);
    BluetoothSerial.print(";");
  }
  if (counter==20){
    sprintf(commandValueString, "%d", (unsigned int)sprocket.getMissionTime());
    BluetoothSerial.print("mi:");
    BluetoothSerial.write(commandValueString);
    //BluetoothSerial.write(sprocket.getMissionTime());
    BluetoothSerial.print(";");
  }  
}

void sendParachuteArmedSensor(byte counter){
  if ((counter % 15)==0){
    if (digitalRead(PARACHUTEARMED_PIN)==HIGH){
      BluetoothSerial.print("ps:1;");
      if (digitalRead(PARACHLED_PIN)==HIGH){
        digitalWrite(PARACHLED_PIN, LOW);
      }
    }else{
      BluetoothSerial.print("ps:0;");
      if (digitalRead(PARACHLED_PIN)==LOW){
        digitalWrite(PARACHLED_PIN, HIGH);
      }
    }    
  }
}

void sendBatteryLevel(byte counter){
  float batteryLevel;
  if ((counter % 25)==0){
    batteryLevel=5*analogRead(BATTERYSENSOR_PIN)*2;
    batteryLevel=batteryLevel/1023;
    #ifdef DEBUG_MODE
    Serial.println("battery level: ");
    Serial.print(batteryLevel,2);
    #endif
    BluetoothSerial.print("ba:");
    dtostrf(batteryLevel, 2, 2, commandValueString); 
    BluetoothSerial.write(commandValueString);
    BluetoothSerial.print(";");
  }
}

void sendTelemetry(byte counter){
  if (counter ==5){
    //Print altitude
    BluetoothSerial.print("al:");
    BluetoothSerial.print(sprocket.getCurrentAltitude((short)bme.readAltitude(SEALEVELPRESSURE_HPA)));
    BluetoothSerial.print(";");
  }
  if (counter ==15){
    //Print temperature
    BluetoothSerial.print("te:");
    commandValueFloat = bme.readTemperature();
    dtostrf(commandValueFloat, 2, 2, commandValueString); 
    BluetoothSerial.write(commandValueString);
    BluetoothSerial.print(";");
  }
  
  if (counter ==25){
    //Print humidity
    BluetoothSerial.write("hu:");
    BluetoothSerial.print(bme.readHumidity());
    BluetoothSerial.write(";");
  }

  if (counter ==35){
    //Print pressure
    BluetoothSerial.write("pr:");
    BluetoothSerial.print(bme.readPressure() / 100.0F);
    BluetoothSerial.write(";");
  }
}


void print_values(){
    #ifdef DEBUG_MODE
    Serial.print("Temperature = ");
    Serial.print(bme.readTemperature());
    Serial.println(" *C");

    Serial.print("Pressure = ");

    Serial.print(bme.readPressure() / 100.0F);
    Serial.println(" hPa");

    Serial.print("Approx. Altitude = ");
    Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
    Serial.println(" m");

    Serial.print("Humidity = ");
    Serial.print(bme.readHumidity());
    Serial.println(" %");

    Serial.println();
    #endif
}

#endif /*I_SPROCKET__C*/

