#ifndef _SPROCKETCONFIG_H_
#define _SPROCKETCONFIG_H_



//Debug mode. Comment out for disable serial debugging
//#define DEBUG_MODE

//Pin configuration
#define READYLED_PIN        4
#define PARACHLED_PIN       5
#define BLUETOOTH_RX        8
#define BLUETOOTH_TX        7
#define LANDINGLED_PIN      10
#define SERVO_PIN           9
#define PARACHUTEARMED_PIN  3
#define BATTERYSENSOR_PIN   A0

//Servo positions
#define SERVO_ARMED       85
#define SERVO_DISARMED    0

//Altimeter defines
//#define SEALEVELPRESSURE_HPA (1013.25)
#define SEALEVELPRESSURE_HPA (1100.25)
#define LIFTOFFTHRESHOLD      1
#define APOGEETHRESHOLD       2
#define ANTINOISEVALUE        3

//State Machine
#define WAIT_BT_CONNECTION  0X00
#define DISARMED            0X01
#define ARMED               0X02
#define WAIT_LAUNCH         0X03
#define WAIT_APOGEE         0X04
#define RELEASE_PARACHUTE   0X05
#define WAIT_LANDING        0X06
#define SPLASHDOWN          0X07
#define WAIT_RESCUE         0X08
#define SEND_TELEMETRY      0X09

//commands
#define HELLO                   0X00
#define SEND_TELEMETRY          0X01
#define SEND_TELEMETRY          0X02
#define SEND_PRESENT_DEVICES    0X03
#define SEND_NO_PRESENT_DEVICES 0X04
#define ARM_ROCKET              0X05
#define DISARM_ROCKET           0X06
#define LAUNCH                  0X07
#define ABORT_LAUNCH            0X08
#define SD_PRESENT              0X09
#define GYRO_PRESENT            0X0A
#define GYRO_VALUE_X            0X0B
#define GYRO_VALUE_Y            0X0C
#define GYRO_VALUE_Z            0X0D
#define IMU_VALUE_X             0X0E
#define IMU_VALUE_Y             0X0F
#define IMU_VALUE_Z             0X10
#define ALTITUDE                0X11
#define TEMPERATURE             0X12
#define PRESSURE                0X13
#define SEND_TELEMETRY_NUMBER   0X14
#define SEND_TELEMETRY          0X15

//general out
#define E_OK                    0x00
#define E_NOT_OK                0x01

//Datalogger config
#define DATALOGGER_NUM_DATA     150
#define WAIT_APOGEE_FREQ        150
#define WAIT_LAND_FREQ          250

//Datalogger value types
#define VALUETYPE_ALTITUDE      0X00
#define VALUETYPE_PARACHUTE     0X01
#define VALUETYPE_LANDING       0X02
#define VALUETYPE_ACCELX        0X03
#define VALUETYPE_ACCELY        0X04
#define VALUETYPE_ACCELZ        0X05

//Datalogger masks
#define CLEAR_TYPE              0x1FFF

//Gyro config
#define GYRO_AVAILABLE          false
#define XAXIS                   0x01
#define YAXIS                   0x02
#define ZAXIS                   0x03
#define SELECTED_AXIS           XAXIS


#endif // _SPROCKETCONFIG_H_