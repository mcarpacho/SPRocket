/******************************************************************************/
/*                                                                            */
/*   Furgalladas Schalter und Sensoren                                        */
/*                                                                            */
/*   All rights reserved. Distribution or duplication without previous        */
/*   written agreement of the owner prohibited.                               */
/*                                                                            */
/******************************************************************************/

/** \file

   SPRocket - Water Rocket onboard control System


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> SPRocket.c                 </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                        </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                   </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho            </td></tr>
   <tr> <td> Date:     </td> <td> 18-MARCH-2018 15:39:56      </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> SPRocket </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/* Bluetooth module HC-05
   RX is digital pin 7 of arduino (connect to TX of HC05)
   TX is digital pin 8 of arduino (connect to RX of HC05)
   Default baudRate: 9600. 
   No AT commands are transmitted, only used vÃ­a software UART
   Pairing code: 1234
*/

/* Sd card
 ** SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4
 */

/* Accelerometer: 
 *    //GND - GND
 *    //VCC - VCC
 *    //SDA - Pin A4
 *    //SCL - Pin A5
 */

/* Servo
 *  Servo pin attached at pin 9
 */

 /* LED
 *  Led pin attached at pin 10
 */

 /*
  * Altimeter - Remember to modify address to match 0x76!!!
  *  //GND - GND
  *  //VCC - VCC
  *  //SDA - Pin A4
  *  //SCL - Pin A5
  */

#ifndef I_SPROCKET_C
#define I_SPROCKET_C

#include <SPRocket.h>
#include <SoftwareSerial.h>
#include <Servo.h>
#include <SPI.h>
//#include <SD.h> //Seems that there is no room for SD library
#include "I2Cdev.h"
//#include "MPU6050_6Axis_MotionApps20.h"
#include "Wire.h"
#include <Adafruit_BME280.h>

//Variables
byte command[10]; //encode received command
byte commandCounter;
byte commandReceived;
static byte sendCommandCounter = 0;
short maxAltitude;


//Bluetooth serial port 
SoftwareSerial BluetoothSerial(BLUETOOTH_RX, BLUETOOTH_TX);

//Servo
Servo sprocketServo;  // create servo object to control a servo

//Altimeter
Adafruit_BME280 bme; // Communicated with I2C

//New rocket
SPRocket sprocket(sprocketServo);

//altitude command
char commandValueString[7];
float commandValueFloat;

void setup() {
  //PinModes
  pinMode(BLUETOOTH_RX, INPUT);
  pinMode(BLUETOOTH_TX, OUTPUT);
  pinMode(LED_PIN, OUTPUT);

  //Init bluetooth module port
  BluetoothSerial.begin(9600);

  //Main debugging port
  Serial.begin(9600);

  //Servo attach
  sprocketServo.attach(SERVO_PIN);
  sprocketServo.write(SERVO_DISARMED);

  //Led pin for simulating apogee
  digitalWrite(LED_PIN,LOW);
  //Altimeter
    bool status;
    status = bme.begin();  
    if (!status) {
        Serial.println("Could not find a valid BME280 sensor, check wiring!");
        while (1);
    }
  
}


void loop() {
  //Check bluetooth buffer
  //command = sprocket.parseCommand(BluetoothSerial);
  /*if (BluetoothSerial.available()){
    while(BluetoothSerial.available()){
      byte bluetoothChar = BluetoothSerial.read();
      Serial.write(bluetoothChar);   
    }   
  }*/
  //check state machine
  switch (sprocket.getStateMachineStatus()) {
    case WAIT_BT_CONNECTION:
      Serial.println("WAIT_BT_CONNECTION state\n");
      if (BluetoothSerial.available()){
        Serial.println("in waiting for bt, received something");
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          Serial.write(bluetoothChar);
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        if (commandReceived==HELLO){
          Serial.println("received hello, ack and update state machine");
          //Received hello. Set state machine
          BluetoothSerial.write("he:0;");
          sprocket.setStateMachineStatus(DISARMED);
        }
      }
      //sprocket.waitConnection(BluetoothSerial);
      //delay(100);
      delay(500);
      break;
    case DISARMED:
      Serial.println("DISARMED state\n");
      if (BluetoothSerial.available()){
        Serial.println("in disarm, received something");
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          Serial.write(bluetoothChar);
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        Serial.print("In disarmed, received command: ");
        Serial.println(commandReceived);
        if (commandReceived==ARM_ROCKET){
          Serial.println("Rocket armed. Ack arm rocket. Ready to launch\n");
          //Received hello. Set state machine
          BluetoothSerial.write("ar:0;");
          sprocket.setStateMachineStatus(ARMED);
          sprocketServo.write(SERVO_ARMED);
        }
      }      
      sendPing(sendCommandCounter);
      delay(500);
      break;
    case ARMED:
      Serial.println("ARMED state\n");
      if (BluetoothSerial.available()){
        Serial.println("in arm, received something");
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          Serial.write(bluetoothChar);
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        if (commandReceived==DISARM_ROCKET){
          //Received disarm. Set state machine
          Serial.println ("Ack disarm rocket\n");
          BluetoothSerial.write("di:0;");
          sprocket.setStateMachineStatus(DISARMED);
          sprocketServo.write(SERVO_DISARMED);
        }
        if (commandReceived==LAUNCH){
          //Received launch. Set state machine
          Serial.println ("Ack launch rocket\n");
          BluetoothSerial.write("la:0;");
          sprocket.setStateMachineStatus(WAIT_LAUNCH);
          sprocket.setGroundAltitude((short)bme.readAltitude(SEALEVELPRESSURE_HPA));
        }
        sendPing(sendCommandCounter);
      }
      delay(500);
      break;
    case WAIT_LAUNCH:
      Serial.println("WAIT_LAUNCH state\n");
      //Abort
      if (BluetoothSerial.available()){
        Serial.println("in waiting for launch, received something");
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          Serial.write(bluetoothChar);
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        if (commandReceived==ABORT_LAUNCH){
          //Received abort launch
          Serial.println ("Ack abort launch rocket\n");
          BluetoothSerial.write("ab:0;");
          sprocket.setStateMachineStatus(ARMED);
          break;
        }
      }
      //print_values();
      /*float altitudeValueFloat;
      altitudeValueFloat = bme.readAltitude(SEALEVELPRESSURE_HPA);
      char altitudeCommand[11];
      char altitudeValueString[5];
      
      dtostrf(altitudeValueFloat, 2, 2, altitudeValueString);  //4 is mininum width, 6 is precision
      strcpy(altitudeCommand, "al:");
      strcat(altitudeCommand, altitudeValueString);
      strcat(altitudeCommand, ";");
      altitudeCommand[9]='\0';
      Serial.print(" Comando a enviar: ");
      Serial.println(altitudeCommand);
      BluetoothSerial.write(altitudeCommand);
      Serial.println ("commando enviado\n");*/
      
      
      /*float altitudeValueFloat;
      altitudeValueFloat = bme.readAltitude(SEALEVELPRESSURE_HPA);
      dtostrf(altitudeValueFloat, 2, 2, altitudeValueString);  //4 is mininum width, 6 is precision
      altitudeCommand="al:";
      strcat(altitudeCommand, altitudeValueString);
      strcat(altitudeCommand, ";");
      int i;
      for (i=0;i<12;i++){
        //Serial.print("byte");Serial.print(i);Serial.print("-");Serial.println(altitudeCommand[i]);
        Serial.print("byteHex");Serial.print(i);Serial.print("-");Serial.println(altitudeCommand[i],HEX);
      }*/
      sendTelemetry(sendCommandCounter);
      //Reset counter
      sendCommandCounter++;
      if (sendCommandCounter ==25) sendCommandCounter =0;
      
      //Serial.println(sprocket.getCurrentAltitude((short)bme.readAltitude(SEALEVELPRESSURE_HPA)));
      if (sprocket.detectLiftOff((short)bme.readAltitude(SEALEVELPRESSURE_HPA))){
        Serial.println ("LiftOff, we have liftOff of Sprocket\n");
        BluetoothSerial.write("li:0;");
        sprocket.setStateMachineStatus(WAIT_APOGEE);
      }
      delay(150);
      break;
    case WAIT_APOGEE:
      Serial.println("WAIT_APOGEE state\n");
      if (sprocket.detectApogee((short)bme.readAltitude(SEALEVELPRESSURE_HPA))){
        Serial.println("Apogee detected!!!\n");
        BluetoothSerial.write("ap:0;");
        sprocket.setStateMachineStatus(RELEASE_PARACHUTE);
      }
      break;
    case RELEASE_PARACHUTE:
      Serial.println("Apogee detected, releasing parachute!!!\n");
      sprocket.setStateMachineStatus(WAIT_LANDING);
      sprocketServo.write(SERVO_DISARMED);
      BluetoothSerial.write("pa:0;");
      digitalWrite(LED_PIN,HIGH);
      break;
    case WAIT_LANDING:
      if (sprocket.detectLanding((short)bme.readAltitude(SEALEVELPRESSURE_HPA))){
        Serial.println("Landing detected!!!\n");
        sprocket.setStateMachineStatus(SPLASHDOWN);
        delay(500);
      }
      break;
    case SPLASHDOWN:
      Serial.println("Splashdown!!! Waiting for rescue\n");
      sprocket.setStateMachineStatus(WAIT_RESCUE);
      BluetoothSerial.write("sp:0;");
      digitalWrite(LED_PIN,LOW);
      delay(500);
      break;
    case WAIT_RESCUE:
      //send max altitude      
      //sprintf(commandValueString, "%d", sprocket.getMaxAltitude());
      sendMissionRecords(sendCommandCounter);
      //BluetoothSerial.write("ma:");
      //BluetoothSerial.write(commandValueString);
      //BluetoothSerial.write(";");
      Serial.write("ma:");
      Serial.write(commandValueString);
      Serial.write(";");
      digitalWrite(LED_PIN,HIGH);
      delay(500);
      digitalWrite(LED_PIN,LOW);
      delay(500);
      break;
  }
  //delay (500);
  /*digitalWrite(10, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay (500);
  digitalWrite(10, LOW);   // turn the LED on (HIGH is the voltage level)
*/
  //Update counters
  sendCommandCounter++;
  if (sendCommandCounter ==25) sendCommandCounter =0;
}

void sendPing(byte counter){
  if ((counter %10)==0){
    BluetoothSerial.print("pi:0;");
  }
}

void sendMissionRecords(byte counter){
  if (counter%15==0){
    sprintf(commandValueString, "%d", sprocket.getMaxAltitude());
    BluetoothSerial.print("ma:");
    BluetoothSerial.write(commandValueString,6);
    BluetoothSerial.print(";");
  }
  if (counter%20==0){
    sprintf(commandValueString, "%d", (int)sprocket.getMissionTime());
    BluetoothSerial.print("mi:");
    BluetoothSerial.write(commandValueString);
    BluetoothSerial.print(";");
  }
  
}

void sendTelemetry(byte counter){
  if (counter ==5){
    //Print altitude
    commandValueFloat = bme.readAltitude(SEALEVELPRESSURE_HPA);
    dtostrf(commandValueFloat, 2, 2, commandValueString); 
    BluetoothSerial.print("al:");
    BluetoothSerial.write(commandValueString,6);
    BluetoothSerial.print(";");
  }
  if (counter ==10){
    //Print temperature
    //Serial.print(bme.readTemperature());
    BluetoothSerial.print("te:");
    commandValueFloat = bme.readTemperature();
    dtostrf(commandValueFloat, 2, 2, commandValueString); 
    BluetoothSerial.write(commandValueString,6);
    //BluetoothSerial.print(bme.readTemperature());
    BluetoothSerial.print(";");
    Serial.write("TEMPERATURA.....:");
    Serial.print(bme.readTemperature());
    Serial.write(";");
  }
  
  if (counter ==15){
    //Print humidity
    BluetoothSerial.write("hu:");
    BluetoothSerial.print(bme.readHumidity());
    BluetoothSerial.write(";");
    Serial.write("HUMIDITY.....:");
    Serial.println(bme.readHumidity());
  }

  if (counter ==20){
    //Print pressure
    BluetoothSerial.write("pr:");
    BluetoothSerial.print(bme.readPressure() / 100.0F);
    BluetoothSerial.write(";");
    Serial.write("HUMIDITY.....:");
    Serial.print(bme.readPressure()/ 100.0F);
    Serial.println("hPa"); 
  }
}


void print_values(){
    Serial.print("Temperature = ");
    Serial.print(bme.readTemperature());
    Serial.println(" *C");

    Serial.print("Pressure = ");

    Serial.print(bme.readPressure() / 100.0F);
    Serial.println(" hPa");

    Serial.print("Approx. Altitude = ");
    Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
    Serial.println(" m");

    Serial.print("Humidity = ");
    Serial.print(bme.readHumidity());
    Serial.println(" %");

    Serial.println();

}



#endif /*I_SPROCKET__C*/

