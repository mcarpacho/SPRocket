/******************************************************************************/
/*                                                                            */
/*   Furgalladas Schalter und Sensoren                                        */
/*                                                                            */
/*   All rights reserved. Distribution or duplication without previous        */
/*   written agreement of the owner prohibited.                               */
/*                                                                            */
/******************************************************************************/

/** \file

   SPRocket - Water Rocket onboard control System


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> SPRocket.c                 </td></tr>
   <tr> <td> Revision: </td> <td> 1.5                        </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                   </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho            </td></tr>
   <tr> <td> Date:     </td> <td> 22-APRIL-2019 15:39:56     </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> SPRocket </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/* Bluetooth module HC-05
   RX is digital pin 8 of arduino (connect to TX of HC05)
   TX is digital pin 7 of arduino (connect to RX of HC05)
   Default baudRate: 9600. 
   No AT commands are transmitted, only used vÃ­a software UART
   Pairing code: 1234
*/

/* Sd card
 ** SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4
 */

/* Accelerometer: Address: 0x68
 *    //GND - GND
 *    //VCC - VCC
 *    //SDA - Pin A4
 *    //SCL - Pin A5
 */

/* Servo
 *  Servo pin attached at pin 9
 */

 /* LEDs
 *  Ready Led attached at pin 4
 *  Parachute sensor Led attached at pin 5
 *  Landing Led attached at pin 10
 */

 /*
  * Altimeter - Remember to modify address to match 0x76!!!
  *  //GND - GND
  *  //VCC - VCC
  *  //SDA - Pin A4
  *  //SCL - Pin A5
  */

  
  /* Sensor pins 
  *
  * Pin 3 => ParachuteArmed pin
  * Pin A0 => Battery sensor
  */

#ifndef I_SPROCKET_C
#define I_SPROCKET_C
#define ARDUINO 101
#include <SPRocket.h>
#include <SoftwareSerial.h>
#include <Servo.h>
#include <SPI.h>
//#include <SD.h> //Seems that there is no room for SD library
#include "I2Cdev.h"
#include <Wire.h>
#include <Adafruit_BME280.h>
//gyro library
#include <MPU6050.h>

//Variables
byte command[10]; //encode received command
byte commandCounter;
byte commandReceived;
static byte sendCommandCounter = 0;


//Bluetooth serial port 
SoftwareSerial BluetoothSerial(BLUETOOTH_RX, BLUETOOTH_TX);

//Servo
Servo sprocketServo;  // create servo object to control a servo

//Altimeter
Adafruit_BME280 bme; // Communicated with I2C

//Gyro
MPU6050 mpu;

//New rocket
SPRocket sprocket(sprocketServo);

//altitude command
char commandValueString[7];
float commandValueFloat;

void setup() {
  int count=0;
  //PinModes  
  pinMode(BLUETOOTH_RX, INPUT);
  pinMode(BLUETOOTH_TX, OUTPUT);
  pinMode(PARACHUTEARMED_PIN,INPUT);
  pinMode(LANDINGLED_PIN, OUTPUT);
  pinMode(PARACHLED_PIN, OUTPUT);
  pinMode(READYLED_PIN, OUTPUT);

  //Init bluetooth module port  
  BluetoothSerial.begin(9600);

  //Main debugging port
  #ifdef DEBUG_MODE
  Serial.begin(9600);
  #endif

  //Servo attach
  sprocketServo.attach(SERVO_PIN);
  sprocketServo.write(SERVO_DISARMED);

  //Initialization leds
  digitalWrite(LANDINGLED_PIN,LOW);
  digitalWrite(PARACHLED_PIN,LOW);
  digitalWrite(READYLED_PIN,LOW);
  //Altimeter
  bool status;
  status = bme.begin();  
  if (!status) {
      #ifdef DEBUG_MODE
      Serial.println("Could not find a valid BME280 sensor, check wiring!");
      #endif
      while (1);
  }  

   //Gyro
  while(!mpu.begin(MPU6050_SCALE_250DPS, MPU6050_RANGE_16G))
  {
    delay(500);
    count++;
    if (count >5) break;
  }
}


void loop() {
  uint8_t gValuesx[50];
  Vector rawAccel, normAccel;
  //check state machine  
  switch (sprocket.getStateMachineStatus()) {
    case WAIT_BT_CONNECTION:


      rawAccel = mpu.readRawAccel();
      normAccel = mpu.readNormalizeAccel();

      
      Serial.print(" Xraw = ");
      Serial.print(rawAccel.XAxis);
      Serial.print(" Yraw = ");
      Serial.print(rawAccel.YAxis);
      Serial.print(" Zraw = ");
    
      Serial.println(rawAccel.ZAxis);
      Serial.print(" Xnorm = ");
      Serial.print(normAccel.XAxis);
      Serial.print(" Ynorm = ");
      Serial.print(normAccel.YAxis);
      Serial.print(" Znorm = ");
      Serial.println(normAccel.ZAxis);
      

    
      #ifdef DEBUG_MODE
      Serial.println("WAIT_BT_CONNECTION state\n");
      #endif
      if (BluetoothSerial.available()){
        #ifdef DEBUG_MODE
        Serial.println("in waiting for bt, received something");
        #endif
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          #ifdef DEBUG_MODE
          Serial.write(bluetoothChar);
          #endif
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        if (commandReceived==HELLO){
          #ifdef DEBUG_MODE
          Serial.println("received hello, ack and update state machine");
          #endif
          //Received hello. Set state machine
          BluetoothSerial.write("he:0;");
          sprocket.setStateMachineStatus(DISARMED);
        }
      }
      delay(500);
      break;
    case DISARMED:
      #ifdef DEBUG_MODE
      Serial.println("DISARMED state\n");
      #endif
      if (BluetoothSerial.available()){
        #ifdef DEBUG_MODE
        Serial.println("in disarm, received something");
        #endif
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          #ifdef DEBUG_MODE
          Serial.write(bluetoothChar);
          #endif
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        #ifdef DEBUG_MODE
        Serial.print("In disarmed, received command: ");        
        Serial.println(commandReceived);
        #endif
        if (commandReceived==ARM_ROCKET){
          #ifdef DEBUG_MODE
          Serial.println("Rocket armed. Ack arm rocket. Ready to launch\n");
          #endif
          //Received hello. Set state machine
          BluetoothSerial.write("ar:0;");
          sprocket.setStateMachineStatus(ARMED);
          sprocketServo.write(SERVO_ARMED);
        }
      }      
      //sendPing(sendCommandCounter);
      delay(500);
      break;
    case ARMED:
      #ifdef DEBUG_MODE
      Serial.println("ARMED state\n");
      #endif
      sendParachuteArmedSensor(sendCommandCounter);
      if (BluetoothSerial.available()){
        #ifdef DEBUG_MODE
        Serial.println("in arm, received something");
        #endif
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          #ifdef DEBUG_MODE
          Serial.write(bluetoothChar);
          #endif
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        if (commandReceived==DISARM_ROCKET){
          //Received disarm. Set state machine
          #ifdef DEBUG_MODE
          Serial.println ("Ack disarm rocket\n");
          #endif
          BluetoothSerial.write("di:0;");
          sprocket.setStateMachineStatus(DISARMED);
          sprocketServo.write(SERVO_DISARMED);
          digitalWrite(PARACHLED_PIN,LOW);
        }
        if (commandReceived==LAUNCH){
          //Received launch. Set state machine
          #ifdef DEBUG_MODE
          Serial.println ("Ack launch rocket\n");
          #endif
          BluetoothSerial.write("la:0;");
          sprocket.setStateMachineStatus(WAIT_LAUNCH);
          digitalWrite(READYLED_PIN,HIGH);
          sprocket.setGroundAltitude((short)bme.readAltitude(SEALEVELPRESSURE_HPA));
        }
        //sendPing(sendCommandCounter);        
      }      
      delay(500);
      break;
    case WAIT_LAUNCH:
      #ifdef DEBUG_MODE
      Serial.println("WAIT_LAUNCH state\n");
      #endif
      //Abort
      if (BluetoothSerial.available()){
        #ifdef DEBUG_MODE
        Serial.println("in waiting for launch, received something");
        #endif
        commandCounter = 0x00;
        while(BluetoothSerial.available()){
          byte bluetoothChar = BluetoothSerial.read();
          #ifdef DEBUG_MODE
          Serial.write(bluetoothChar);
          #endif
          command[commandCounter]=bluetoothChar;
          commandCounter +=1;
          if (bluetoothChar==';') break;
        }
        commandReceived=sprocket.parseReceivedCommand(command);
        if (commandReceived==ABORT_LAUNCH){
          //Received abort launch
          #ifdef DEBUG_MODE
          Serial.println ("Ack abort launch rocket\n");
          #endif
          BluetoothSerial.write("ab:0;");
          sprocket.setStateMachineStatus(ARMED);
          digitalWrite(READYLED_PIN,LOW);
          break;
        }
      }
      //print_values();
      sendTelemetry(sendCommandCounter);
      sendParachuteArmedSensor(sendCommandCounter); 
      //Reset counter
      sendCommandCounter++;
      if (sendCommandCounter ==25) sendCommandCounter =0;
      
      //Serial.println(sprocket.getCurrentAltitude((short)bme.readAltitude(SEALEVELPRESSURE_HPA)));
      if (sprocket.detectLiftOff((short)bme.readAltitude(SEALEVELPRESSURE_HPA))){
        #ifdef DEBUG_MODE
        Serial.println ("LiftOff, we have liftOff of Sprocket\n");
        #endif
        BluetoothSerial.write("li:0;");
        sprocket.setStateMachineStatus(WAIT_APOGEE);
      }
      delay(150);
      break;
    case WAIT_APOGEE:
      #ifdef DEBUG_MODE
      Serial.println("WAIT_APOGEE state\n");
      #endif
      if (sprocket.detectApogee((short)bme.readAltitude(SEALEVELPRESSURE_HPA))){
        #ifdef DEBUG_MODE
        Serial.println("Apogee detected!!!\n");
        #endif
        BluetoothSerial.write("ap:0;");
        sprocket.setStateMachineStatus(RELEASE_PARACHUTE);
      }
      break;
    case RELEASE_PARACHUTE:
      #ifdef DEBUG_MODE
      Serial.println("Apogee detected, releasing parachute!!!\n");
      #endif
      sprocket.setStateMachineStatus(WAIT_LANDING);
      //sprocket.setClimbTime();
      sprocketServo.write(SERVO_DISARMED);
      BluetoothSerial.write("pa:0;");
      digitalWrite(LANDINGLED_PIN,HIGH);
      break;
    case WAIT_LANDING:
      if (sprocket.detectLanding((unsigned short)bme.readAltitude(SEALEVELPRESSURE_HPA))){
        #ifdef DEBUG_MODE
        Serial.println("Landing detected!!!\n");
        #endif
        sprocket.setStateMachineStatus(SPLASHDOWN);
        delay(500);
      }
      break;
    case SPLASHDOWN:
      #ifdef DEBUG_MODE
      Serial.println("Splashdown!!! Waiting for rescue\n");
      #endif
      sprocket.setStateMachineStatus(WAIT_RESCUE);
      BluetoothSerial.write("sp:0;");
      digitalWrite(LANDINGLED_PIN,LOW);
      delay(500);
      break;
    case WAIT_RESCUE:
      //send max altitude      
      //sprintf(commandValueString, "%d", sprocket.getMaxAltitude());
      sendMissionRecords(sendCommandCounter);
      digitalWrite(LANDINGLED_PIN,HIGH);
      delay(500);
      digitalWrite(LANDINGLED_PIN,LOW);
      delay(500);
      break;
  }

  //Send battery level if state is other than 
  if ((sprocket.getStateMachineStatus()!=WAIT_BT_CONNECTION) && (sprocket.getStateMachineStatus()!=WAIT_APOGEE) &&
  (sprocket.getStateMachineStatus()!=RELEASE_PARACHUTE) && (sprocket.getStateMachineStatus()!=WAIT_LANDING)  
  ){
    sendBatteryLevel(sendCommandCounter);
  }
  //Update counters
  sendCommandCounter++;
  if (sendCommandCounter ==25) sendCommandCounter =0;
}

void sendPing(byte counter){
  if ((counter %10)==0){
    BluetoothSerial.print("pi:0;");
  }
}

void sendMissionRecords(byte counter){
  if (counter%10==0){
    //sprintf(commandValueString, "%d", sprocket.getClimbTime());
    BluetoothSerial.print("ct:");
    BluetoothSerial.write(commandValueString);
    BluetoothSerial.print(";");
  }
  if (counter%15==0){
    sprintf(commandValueString, "%d", sprocket.getMaxAltitude());
    BluetoothSerial.print("ma:");
    BluetoothSerial.write(commandValueString);
    BluetoothSerial.print(";");
  }
  if (counter%20==0){
    sprintf(commandValueString, "%d", (unsigned int)sprocket.getMissionTime());
    BluetoothSerial.print("mi:");
    BluetoothSerial.write(commandValueString);
    //BluetoothSerial.write(sprocket.getMissionTime());
    BluetoothSerial.print(";");
  }  
}

void sendParachuteArmedSensor(byte counter){
  if (counter % 21){
    if (digitalRead(PARACHUTEARMED_PIN)==HIGH){
      BluetoothSerial.print("ps:1;");
      if (digitalRead(PARACHLED_PIN)==HIGH){
        digitalWrite(PARACHLED_PIN, LOW);
      }
    }else{
      BluetoothSerial.print("ps:0;");
      if (digitalRead(PARACHLED_PIN)==LOW){
        digitalWrite(PARACHLED_PIN, HIGH);
      }
    }    
  }
}

void sendBatteryLevel(byte counter){
  float batteryLevel;
  if ((counter %10)==0){
    batteryLevel=5*analogRead(BATTERYSENSOR_PIN)*2;
    batteryLevel=batteryLevel/1023;
    #ifdef DEBUG_MODE
    Serial.println("sensor battery level: ");
    Serial.print(batteryLevel,2);
    #endif
    BluetoothSerial.print("ba:");
    dtostrf(batteryLevel, 2, 2, commandValueString); 
    BluetoothSerial.write(commandValueString);
    BluetoothSerial.print(";");
  }
}

void sendTelemetry(byte counter){
  if (counter ==5){
    //Print altitude
    BluetoothSerial.print("al:");
    BluetoothSerial.print(sprocket.getCurrentAltitude((short)bme.readAltitude(SEALEVELPRESSURE_HPA)));
    BluetoothSerial.print(";");
  }
  if (counter ==10){
    //Print temperature
    BluetoothSerial.print("te:");
    commandValueFloat = bme.readTemperature();
    dtostrf(commandValueFloat, 2, 2, commandValueString); 
    BluetoothSerial.write(commandValueString);
    BluetoothSerial.print(";");
  }
  
  if (counter ==15){
    //Print humidity
    BluetoothSerial.write("hu:");
    BluetoothSerial.print(bme.readHumidity());
    BluetoothSerial.write(";");
  }

  if (counter ==20){
    //Print pressure
    BluetoothSerial.write("pr:");
    BluetoothSerial.print(bme.readPressure() / 100.0F);
    BluetoothSerial.write(";");
  }
}


void print_values(){
    #ifdef DEBUG_MODE
    Serial.print("Temperature = ");
    Serial.print(bme.readTemperature());
    Serial.println(" *C");

    Serial.print("Pressure = ");

    Serial.print(bme.readPressure() / 100.0F);
    Serial.println(" hPa");

    Serial.print("Approx. Altitude = ");
    Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
    Serial.println(" m");

    Serial.print("Humidity = ");
    Serial.print(bme.readHumidity());
    Serial.println(" %");

    Serial.println();
    #endif
}

#endif /*I_SPROCKET__C*/

